﻿const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const webpack = require("webpack");

module.exports = (env, args) => {
    const mode = args && args.mode ? args.mode :
        env && env.processMode ? env.processMode :
            "development";

    return {
        mode: mode,
        optimization: {
            minimizer: [
                new UglifyJsPlugin({
                    cache: true,
                    parallel: true,
                    uglifyOptions: {
                        compress: true,
                        ecma: 6,
                        mangle: true
                    },
                    sourceMap: true
                })
            ]
        },
        entry: {
            "main": "./wwwroot/source/main.jsx"
        },
        output: {
            path: path.resolve(__dirname, "wwwroot/dist"),
            filename: "[name]_bundle.js",
            publicPath: "dist/"
        },
        plugins: [
            new webpack.ProvidePlugin({
                "Object.assign": "core-js/fn/object/assign",
                "Promise": "core-js/fn/promise"
            }),
            new webpack.DefinePlugin({
                "processMode": JSON.stringify(mode)
            }),
            new MiniCssExtractPlugin({
                filename: "[name]_style.css"
            })
        ],
        externals: {
            react: "React",
            "react-dom": "ReactDOM"
        },
        module: {
            rules: [
                {
                    test: /\.(svg)$/,
                    loader: 'url-loader?limit=100000'
                },

                {
                    test: /\.(ttf|eot|woff(2)?)$/,
                    use: [
                        {
                            loader: "file-loader",
                            options: {
                                name: '[name].[ext]',
                                publicPath: (url) => {
                                    return `/dist/${/dxicons/.test(url) ? "icons" : "fonts"}/${url}`;
                                },
                                outputPath: (url) => {
                                    return `${/dxicons/.test(url) ? "icons" : "fonts"}/${url}`;
                                }
                            }
                        }
                    ]
                },
                {
                    test: /\.(png|jpg|jpeg|gif)$/,
                    use: [
                        {
                            loader: "file-loader",
                            options: {
                                name: '[name].[ext]',
                                publicPath: (url) => {
                                    return `/dist/images/${url}`;
                                },
                                outputPath: 'images/'
                            }
                        }
                    ]
                },
                {
                    test: /\.scss$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        "css-loader",
                        {
                            loader: "sass-loader",
                            options: {
                                includePaths: ['./node_modules']
                            }
                        }
                    ]
                },
                {
                    test: /\.css$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        "css-loader"
                    ]
                },
                {
                    test: /\.(js|jsx)$/,
                    use: {
                        loader: "babel-loader",
                        options: {
                            presets: [
                                "@babel/preset-env",
                                "@babel/preset-react"
                            ]
                        }
                    }
                }
            ]
        }
    };
}