﻿namespace ZipArchiver.Models.DTO
{
    public class CompressResult
    {
        public string ZipPath { get; set; }
        public string Description { get; set; }
    }
}
