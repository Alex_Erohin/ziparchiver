﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;
using ZipArchiver.Models.DTO;

namespace ZipArchiver.Models
{
    public interface IFileService
    {
        /// <summary>
        /// Загрузка файлов на сервер
        /// </summary>
        Task<UploadFile> UploadFile(IFormFile formFile);

        /// <summary>
        /// Сжатие файлов в архив
        /// </summary>
        CompressResult CompressFiles(IEnumerable<UploadFile> files);
    }

    public class FileService : IFileService
    {
        private readonly IHostingEnvironment _hostingEnvironment;

        public FileService(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        public async Task<UploadFile> UploadFile(IFormFile formFile)
        {
            var outputPath = Path.Combine(_hostingEnvironment.WebRootPath, "upload\\");
            var filePath = Path.Combine(outputPath, Path.GetFileName(formFile.FileName));

            if (!Directory.Exists(outputPath))
                Directory.CreateDirectory(outputPath);

            using (var stream = File.Create(filePath))
            {
                await formFile.CopyToAsync(stream);
            }

            return new UploadFile
            {
                FileName = formFile.FileName,
                Path = filePath,
                Description = $"Исходный размер: {Math.Round(formFile.Length / (1024m * 1024), 2)} mb"
            };
        }

        public CompressResult CompressFiles(IEnumerable<UploadFile> files)
        {
            var timeBeg = DateTime.Now;
            var zipPath = Path.Combine(_hostingEnvironment.WebRootPath, "upload\\");
            var zipName = $"{Guid.NewGuid()}.zip";
            var zipLocation = Path.Combine(zipPath, zipName);

            using (var zipArchive = ZipFile.Open(zipLocation, ZipArchiveMode.Update))
            {
                Parallel.ForEach(files, file =>
                {
                    var fileInfo = new FileInfo(file.Path);
                    zipArchive.CreateEntryFromFile(fileInfo.FullName, fileInfo.Name);
                });
            }

            var zipInfo = new FileInfo(zipLocation);

            return new CompressResult
            {
                ZipPath = $"/upload/{zipName}",
                Description = $"Время сжатия: {Math.Round((DateTime.Now - timeBeg).TotalMilliseconds)} мс, размер {zipInfo.Length / (1024 * 1024)} mb"
            };
        }
    }
}
