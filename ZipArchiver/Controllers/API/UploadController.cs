﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ZipArchiver.Models;
using ZipArchiver.Models.DTO;

namespace ZipArchiver.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class UploadController : ControllerBase
    {
        readonly IFileService _fileService;

        public UploadController(IFileService fileService)
        {
            _fileService = fileService;
        }

        // POST: api/Upload
        [HttpPost]
        public async Task<UploadFile> Post()
        {
            var files = HttpContext.Request.Form?.Files;
            var outputFiles = new List<UploadFile>();
            var uploadFile = new UploadFile();

            if (files.Any())
            {
                uploadFile = await _fileService.UploadFile(files.FirstOrDefault());
            }

            return uploadFile;
        }

        [HttpPut]
        public CompressResult Put([FromBody]IEnumerable<UploadFile> files)
        {
            return _fileService.CompressFiles(files);
        }
    }
}