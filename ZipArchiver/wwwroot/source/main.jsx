﻿import React from "react";
import ReactDom from "react-dom";
import "./main.scss";
import 'devextreme/dist/css/dx.common.css';
import 'devextreme/dist/css/dx.material.blue.light.css';
import { Button, FileUploader, List } from "devextreme-react";
import notify from "devextreme/ui/notify";
import axios from 'axios';

(function (processMode, module) {

    class App extends React.Component {
        constructor(props) {
            super(props);

            this.onUploaded = this.onUploaded.bind(this);
            this.onCompressFiles = this.onCompressFiles.bind(this);

            this.state = {
                files: [],
                showLoader: false,
                compressedFiles: []
            };
        }

        onUploaded(e) {
            this.setState({
                files: [
                    ...this.state.files,
                    JSON.parse(e.request.responseText)
                ]
            });
        }

        onCompressFiles() {
            const targetFiles = this.state.files.map(f => ({ ...f }));

            this.setState({
                showLoader: true,
                files: []
            }, () => {
                axios.put('/api/upload/', targetFiles)
                    .then(res => {
                        this.setState({
                            compressedFiles: [
                                ...this.state.compressedFiles,
                                res.data
                            ]
                        });
                    })
                    .catch(e => notify(e.response && e.response.statusText ? e.response.statusText : "Ошибка на сервере",
                        "error", 5000))
                    .finally(() => {
                        this.setState({
                            showLoader: false
                        });
                    });
            });
        }

        onRenderCompressedItem(item) {
            return (
                <div className="compressed-item">
                    <a href={item.zipPath}>Ссылка</a>
                    <span>{item.description}</span>
                </div>
            );
        }

        render() {
            return (
                <div className="app">
                    <div className="uploader">
                        <FileUploader multiple selectButtonText="Загрузить файлы" name="file" uploadUrl="/api/upload"
                            uploadMethod="POST" height="100%" width="100%" onUploaded={this.onUploaded}
                            disabled={this.state.showLoader}
                        />
                    </div>
                    <div className="result">
                        <Button disabled={!this.state.files.length} type="default" onClick={this.onCompressFiles}>
                            Сжать файлы ({this.state.files.length} шт.)
                        </Button>
                        <div style={{ display: this.state.showLoader ? "block" : "none" }} className="load-status">
                            Сжатие...
                        </div>
                        <List dataSource={this.state.compressedFiles} itemRender={this.onRenderCompressedItem} />
                    </div>
                </div>
            );
        }
    }

    ReactDom.render(<App />, document.getElementById("app"));

    if (processMode === "development")
        module.hot.accept();
})(processMode, module);